<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MasterSampelAirController;
use App\Http\Controllers\MasterDiagnosaController;
use App\Http\Controllers\TransaksiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [ApiController::class, 'authenticate']);
Route::post('register', [ApiController::class, 'register']);


Route::group(['middleware' => ['jwt.verify']], function() {

    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get_user', [ApiController::class, 'get_user']);
    Route::put('site_user/update/{id}', [UserController::class, 'update_site']);

    Route::group(['prefix' => 'mastersampelair'], function () {
        Route::get('read', [MasterSampelAirController::class, 'show']);
        Route::post('create', [MasterSampelAirController::class, 'store']);
        Route::put('update/{id}', [MasterSampelAirController::class, 'update']);
        Route::delete('delete/{id}', [MasterSampelAirController::class, 'destroy']);
    });

    Route::group(['prefix' => 'masterdiagnosa'], function () {
        Route::get('read', [MasterDiagnosaController::class, 'show']);
        Route::post('create', [MasterDiagnosaController::class, 'store']);
        Route::put('update/{id}', [MasterDiagnosaController::class, 'update']);
        Route::delete('delete/{id}', [MasterDiagnosaController::class, 'destroy']);
    });

    Route::group(['prefix' => 'transaksi'], function () {
        Route::get('read', [TransaksiController::class, 'show']);
        Route::post('create', [TransaksiController::class, 'store']);
        Route::put('update/{id}', [TransaksiController::class, 'update']);
        Route::delete('delete/{id}', [TransaksiController::class, 'destroy']);
    });


});

// let this code at the end of line
Route::any('{path}', function() {
    return response()->json([
        'status' => false,
        'code' => 404,
        'message' => 'Route Not Found, Please Check Your Code'
    ], 404);
})->where('path', '.*');
