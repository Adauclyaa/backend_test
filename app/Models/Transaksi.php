<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;

    protected $table = 'transaksi_formulir';
    protected $guarded  = ['id'];
    protected $fillable = [
        'userid',
        'customerid',
        'nomor_registrasi',
        'tanggal_registrasi',
        'status',
    ];
}
