<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiDiagnosa extends Model
{
    use HasFactory;

    protected $table = 'transaksi_diagnosa';
    protected $guarded  = ['id'];
    protected $fillable = [
        'transaksiid',
        'diagnosaid',
        'qty',
        'total',
    ];
}
