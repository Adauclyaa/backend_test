<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiSampel extends Model
{
    use HasFactory;

    protected $table = 'transaksi_sampel_air';
    protected $guarded  = ['id'];
    protected $fillable = [
        'transaksiid',
        'sampelid',
        'qty',
        'total',
    ];
}
