<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterSite extends Model
{
    use HasFactory;

    const CREATED_AT = 'created_datetime';
    const UPDATED_AT = 'lastupdate_datetime';

    protected $table = 'mssite';
    protected $guarded  = ['id'];
    protected $fillable = [
        'siteid',
        'sitename',
        'sitetype',
        'companyid',
        'sitecompanyname',
        'siteaddress',
        'sitephone',
        'sitefax',
        'sitefilename',
        'status',
        'created_user',
        'created_datetime',
        'lastupdate_user',
        'lastupdate_datetime'
    ];
}
