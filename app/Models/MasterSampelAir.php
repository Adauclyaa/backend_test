<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterSampelAir extends Model
{
    use HasFactory;

    protected $table = 'master_sampel_air';
    protected $guarded  = ['id'];
    protected $fillable = [
        'id',
        'parameter',
        'tarif',
    ];
}
