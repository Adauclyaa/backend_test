<?php

namespace App\Http\Controllers;

use App\Models\TransaksiSampel;
use App\Http\Requests\StoreTransaksiSampelRequest;
use App\Http\Requests\UpdateTransaksiSampelRequest;

class TransaksiSampelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksiSampelRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaksiSampelRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiSampel  $transaksiSampel
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiSampel $transaksiSampel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransaksiSampel  $transaksiSampel
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiSampel $transaksiSampel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksiSampelRequest  $request
     * @param  \App\Models\TransaksiSampel  $transaksiSampel
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransaksiSampelRequest $request, TransaksiSampel $transaksiSampel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiSampel  $transaksiSampel
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiSampel $transaksiSampel)
    {
        //
    }
}
