<?php

namespace App\Http\Controllers;

use JWTAuth;
use App\Models\User;
use App\Models\UserGroup;
use App\Models\Groups;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
    public function register(Request $request)
    {
        $data = $request->only('name', 'companyname', 'storename', 'siteid', 'position', 'email', 'phone', 'password', 'categoryid');
        $validator = Validator::make($data, [
            'name' => 'required|string',
            'companyname' => 'required|string|unique:tenantcompany',
            'storename' => 'required|string',
            'siteid' => 'required|string',
            'position' => 'required|string',
            'phone' => 'required|numeric',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|max:50',
            'categoryid' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $split_email = explode('@', $request->email);
        $username = $split_email[0];

        $tenantcompany = TenantCompany::create([
            'categoryid' => $request->categoryid,
            'companyname' => $request->companyname,
            'attention' => $request->name,
            'email' => $request->email,
        	'phone' => $request->phone
        ]);

        $idtenantcompany = $tenantcompany->id;
        $tenant = Tenant::create([
            'storename' => $request->storename,
            'phone' => $request->phone,
            'siteid' => $request->siteid,
            'tenantcompanyid' => $idtenantcompany
        ]);

        $user = User::create([
            'companyname' => $request->companyname,
            'phone' => $request->phone,
            'siteid' => $request->siteid,
            'position' => $request->position,
        	'username' => $username,
        	'name' => $request->name,
        	'email' => $request->email,
        	'password' => bcrypt($request->password),
            'tenantid' => $tenant->id,
            'tenantcompanyid' => $idtenantcompany,
            'status' => 'A',
            'type' => 'main'
        ]);

        $users_group = UserGroup::create([
            'user_id' => $user->id,
            'group_id' => Groups::where('name', 'tenant')->first()->id
        ]);

        //User created, return success response
        return response()->json([
            'success' => true,
            'message' => 'User created successfully',
            'data' => $user
        ], Response::HTTP_OK);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');
        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated
        //Crean token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }

 		//Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
    }

    public function logout(Request $request)
    {
        //valid credential
        $validator = Validator::make($request->only('token'), [
            'token' => 'required'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

		//Request is validated, do logout
        try {
            JWTAuth::invalidate($request->token);

            return response()->json([
                'success' => true,
                'message' => 'User has been logged out'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                'success' => false,
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get_user(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);

        $user = JWTAuth::authenticate($request->token);

        $data_role = User::join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->join('groups', 'groups.id', '=', 'users_groups.group_id')
        ->select('groups.name', 'users_groups.user_id')
        ->where('users.id', $user->id)
        ->get();
        $user->role = $data_role[0]->name;

        return response()->json([$user]);
    }

}
