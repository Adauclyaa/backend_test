<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\UserGroup;
use App\Models\AdminSite;
use App\Models\Groups;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class UserController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
        $data = $request->only(
            "name",
            "username",
            "password",
            "email",
            "role",
            "department",
            "company",
            "approver",
            "notification",
            "status"
        );

        $validator = Validator::make($data, [
            "name" => 'required|string',
            "username" => 'required|string',
            "password" => 'required|string|min:8|max:50',
            "email" => 'required|email|unique:users',
            "role" => 'required|string',
            "department" => 'required|array',
            "company" => 'required|array',
            "approver" => 'required|string',
            "notification" => 'required|string',
            "status" => 'required|string|max:1',
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->messages()], 200);
        }

        $user = JWTAuth::parseToken()->authenticate();
        $group = Groups::where('name', $request->role)->first();

        $data_user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'siteid' => $request->company[0],
            'status' => $request->status,
            'created_by' => $user->id,
            'updated_by' => $user->id,
            'password' => bcrypt($request->password),
            'approver' => $request->approver,
            'notification' => $request->notification,
            'departmentid' => $request->department[0],
            'type' => 'main'
        ]);

        foreach ($request->department as $key => $value) {
            $usergroup = UserGroup::create([
                'user_id' => $data_user->id,
                'group_id' => $group->id,
                'departmentid' => $value,
            ]);
        }

        foreach ($request->company as $key => $value) {
            $usergroup = AdminSite::create([
                'userid' => $data_user->id,
                'siteid' => $value
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'User created successfully',
            'data' => $data_user
        ], Response::HTTP_OK);
    }

    public function storeekternal(Request $request)
    {
        $data = $request->only(
            'name',
            'phone',
            'username',
            'position',
            'email',
            'siteid',
            'status',
            'created_at',
            'updated_at',
            'tenantid',
            'tenantcompanyid',
            'password',
        );

        $validator = Validator::make($data, [
            'name' => 'required|string',
            'companyname' => 'required|string',
            'phone' => 'required|numeric',
            'username' => 'required|string',
            'position' => 'required|string',
            'email' => 'required|email|unique:users',
            'siteid' => 'required|string',
            'refid' => 'required|numeric',
            'refdefault' => 'required|numeric',
            'status' => 'required|string|max:1',
            'created_user' => 'required|string',
            'updated_user' => 'required|string',
            'tenantid' => 'required|numeric',
            'tenantcompanyid' => 'required|numeric',
            'password' => 'required|string|min:8|max:50',
        ]);

        if ($validator->fails()){
            return response()->json(['error' => $validator->messages()], 200);
        }

        $department = User::create([
            'name' => $request->name,
            'companyname' => $request->companyname,
            'phone' => $request->phone,
            'username' => $request->username,
            'position' => $request->position,
            'email' => $request->email,
            'siteid' => $request->siteid,
            'refid' => $request->refid,
            'refdefault' => $request->refdefault,
            'status' => $request->status,
            'created_by' => $request->created_by,
            'updated_by' => $request->updated_by,
            'tenantid' => $request->tenantid,
            'tenantcompanyid' => $request->tenantcompanyid,
            'password' => bcrypt($request->password),
        ]);

        return response()->json([
            'status' => true,
            'message' => 'User created successfully',
            'data' => $department
        ], Response::HTTP_OK);
    }

    public function show()
    {
        $addRelationConstraint = false;
        $data = QueryBuilder::for(User::class)
        ->join('users_groups', 'users_groups.user_id', 'users.id')
        ->join('groups', 'groups.id', 'users_groups.group_id')
        ->join('msdepartment', 'users.departmentid', 'msdepartment.departmentid')
        ->join('mssite', 'users.siteid', 'mssite.siteid')
        ->select(
            'users.*',
            'groups.name AS role',
            'groups.description AS role_name',
            'msdepartment.departmentid',
            'msdepartment.departmentname',
            'mssite.siteid',
            'mssite.sitename'
        )
        ->allowedFilters([
            // AllowedFilter::partial('companyname', 'tenantcompany.companyname'),
            AllowedFilter::exact('id'),
            AllowedFilter::exact('siteid'),
        ])
        ->orderBy('id', 'ASC')
        ->get();

        return response()->json($data);
    }

    public function showeksternal($param=null)
    {
        if($param == 'all'){
            $data = DB::table('view_users_eksternal')->get();
        }else{
            $split_param = explode('&', $param);
            for($i=0; $i<count($split_param); $i++){
                $split_value = explode('=', $split_param[$i]);
                $kondisi[$split_value[0]] = $split_value[1];
            }

            $data = DB::table('view_users_eksternal')->where($kondisi)->get();
        }
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        $validator = Validator::make($data, [
            'name' => 'string',
            'phone' => 'numeric',
            'username' => 'string',
            'position' => 'string',
            'email' => 'email',
            'status' => 'string|max:1',
            'tenantid' => 'numeric',
            'tenantcompanyid' => 'numeric',
            'password' => 'string|min:8|max:50|nullable',
            "role" => 'string',
            "department" => 'array',
            "company" => 'array',
            "approver" => 'string',
            "notification" => 'string',
        ]);

        $cek_email = User::where('email', $request->email)->where('id', '!=', $id)->count();
        $cek_username = User::where('username', $request->username)->where('id', '!=', $id)->count();
        if($cek_email >= 1) {
            return response()->json(['error' => ['email' => 'The email has already been taken.']], 200);
        }
        if($cek_username >= 1) {
            return response()->json(['error' => ['username' => 'The username has already been taken.']], 200);
        }
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        try {
            $listuser = User::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        $user_login = JWTAuth::parseToken()->authenticate();

        $param = [
            'name' => $request->name,
            'phone' => $request->phone,
            'username' => $request->username,
            'position' => $request->position,
            'email' => $request->email,
            'siteid' => $request->company[0],
            'status' => $request->status,
            'updated_by' => $user_login->id,
            'tenantid' => $request->tenantid,
            'tenantcompanyid' => $request->tenantcompanyid,
            'approver' => $request->approver,
            'notification' => $request->notification,
        ];
        (!empty($request->password) ?  $param['password'] = bcrypt($request->password) : '' );
        $user = $listuser->update($param);

        $group = Groups::where('name', $request->role)->first();
        try {
            $user_group = UserGroup::where('user_id', $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }
        $user_group->delete();
        foreach ($request->department as $key => $value) {
            UserGroup::create([
                'user_id' => $id,
                'group_id' => $group->id,
                'departmentid' => $value,
            ]);
        }

        try {
            $admin_site = AdminSite::where('userid', $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }
        $admin_site->delete();
        foreach ($request->company as $key => $value) {
            AdminSite::create([
                'userid' => $id,
                'siteid' => $value
            ]);
        }

        if($user){
            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'message' => 'User updated successfully',
                'data' => $user
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_OK,
                'message' => 'User updated failed',
                'data' => $user
            ], Response::HTTP_OK);
        }
    }

    public function update_site(Request $request, $id)
    {
         //Validate data
         $data = $request->only('siteid');
         $validator = Validator::make($data, [
             'siteid' => 'string',
         ]);

         //Send failed response if request is not valid
         if ($validator->fails()) {
             return response()->json(['error' => $validator->messages()], 200);
         }

         $user = User::findOrFail($id);
         $test = $user->update([
             'siteid' => $request->siteid,
         ]);

         $token = str_replace('Bearer', '', $_SERVER['HTTP_AUTHORIZATION']);
         $newToken = JWTAuth::authenticate_token(trim($token));

         JWTAuth::invalidate($token);
         return response()->json([
            'status' => true,
            'code' => 200,
            'token' => $newToken,
        ]);
    }

    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        try {
            $admin_site = AdminSite::where('userid', $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        try {
            $user_group = UserGroup::where('user_id', $id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }


        $user->delete();
        $admin_site->delete();
        $user_group->delete();

        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK,
            'message' => 'User deleted successfully'
        ], Response::HTTP_OK);
    }

    public function destroyeksternal($id)
    {
        try {
            $user = User::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        $user->delete();
        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK,
            'message' => 'User deleted successfully'
        ], Response::HTTP_OK);
    }
}
