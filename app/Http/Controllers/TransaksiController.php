<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\TransaksiDiagnosa;
use App\Models\TransaksiSampel;
use App\Models\Customer;
use App\Http\Requests\StoreTransaksiRequest;
use App\Http\Requests\UpdateTransaksiRequest;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksiRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaksiRequest $request)
    {
        $data = $request->only('customerid', 'tanggal_registrasi', 'status');
        $validator = Validator::make($data, [
            'customerid' => 'numeric',
            'tanggal_registrasi' => 'string',
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $user_login = JWTAuth::parseToken()->authenticate();
        $site = Transaksi::create([
            'userid' => $user_login->id,
            'customerid' => $request->customerid,
            'tangga_registrasi' => $request->tangga_registrasi,
            'status' => 'draft'
        ]);

        try {
            $transaksi = Transaksi::findOrFail($site->id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        $nomor = 'R00'.$site->id;
        $update_site = $transaksi->update([
            'nomor_registrasi' => $nomor,
        ]);

        //Product created, return success response
        return response()->json([
            'status' => true,
            'message' => 'Add form successfully',
            'data' => $site
        ], Response::HTTP_OK);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function show(Transaksi $transaksi)
    {
        $data = QueryBuilder::for(Transaksi::class)
        ->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('userid'),
            AllowedFilter::exact('customerid'),
            AllowedFilter::exact('nomor_registrasi'),
            AllowedFilter::exact('status'),
        ])
        ->get();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaksi $transaksi)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksiRequest  $request
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransaksiRequest $request, Transaksi $transaksi, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'nama' => 'string',
            'alamat' => 'string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        try {
            $transaksi = Transaksi::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }


        foreach($request->data_diagnosa as $value){
            $transaksi_diagnosa_cek = TransaksiDiagnosa::where('diagnosaid', $value['id'])->where('transaksiid', $id)->get();
            if(!isset($transaksi_diagnosa_cek[0])){
                $transaksi_diagnosa = TransaksiDiagnosa::create([
                    'transaksiid' => $id,
                    'diagnosaid' => $value['id'],
                    'qty' => $value['qty'],
                    'total' => $value['jumlah']
                ]);
            }
        }

        foreach($request->data_sampel as $value){
            $transaksi_sampel_cek = TransaksiSampel::where('sampelid', $value['id'])->where('transaksiid', $id)->get();
            if(!isset($transaksi_sampel_cek[0])){
                $transaksi_sampel = TransaksiSampel::create([
                    'transaksiid' => $id,
                    'sampelid' => $value['id'],
                    'qty' => $value['qty'],
                    'total' => $value['jumlah']
                ]);
            }
        }


        $customer_cek = Customer::where('nama', $request->nama)->where('alamat', $request->alamat)->get();
        if(!isset($customer_cek[0])){
            $customer = Customer::create([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
            ]);
        }

        $update_transaksi = $transaksi->update([
            'tanggal_registrasi' => $request->tanggal,
            'status' => 'completed',
            'customerid' => $customer->id,
        ]);


        if($update_transaksi){
            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'message' => 'Transaksi updated successfully',
                'data' => $update_transaksi
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_OK,
                'message' => 'Transaksi updated failed',
                'data' => $update_transaksi
            ], Response::HTTP_OK);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Transaksi  $transaksi
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transaksi $transaksi)
    {
        //
    }
}
