<?php

namespace App\Http\Controllers;

use App\Models\UserGroup;
use Illuminate\Http\Request;
use JWTAuth;
use DB;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class UserGroupController extends Controller
{
    public function index()
    {
    }
    public function show()
    {
        $addRelationConstraint = false;
        $data = QueryBuilder::for(UserGroup::class)
        ->join('groups', 'users_groups.group_id', 'groups.id')
        ->allowedFilters([
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('group_id'),
        ])
        ->select(
            'users_groups.*',
            'groups.name AS role',
            'groups.description AS role_description')
        ->get();

        return response()->json($data);
    }
}
