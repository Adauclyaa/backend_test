<?php

namespace App\Http\Controllers;

use App\Models\MasterSampelAir;
use App\Http\Requests\StoreMasterSampelAirRequest;
use App\Http\Requests\UpdateMasterSampelAirRequest;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class MasterSampelAirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMasterSampelAirRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMasterSampelAirRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterSampelAir  $masterSampelAir
     * @return \Illuminate\Http\Response
     */
    public function show(MasterSampelAir $masterSampelAir)
    {
        $data = QueryBuilder::for(MasterSampelAir::class)
        ->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('parameter'),
            AllowedFilter::exact('tarif'),
        ])
        ->get();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterSampelAir  $masterSampelAir
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterSampelAir $masterSampelAir)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMasterSampelAirRequest  $request
     * @param  \App\Models\MasterSampelAir  $masterSampelAir
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMasterSampelAirRequest $request, MasterSampelAir $masterSampelAir)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterSampelAir  $masterSampelAir
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterSampelAir $masterSampelAir)
    {
        //
    }
}
