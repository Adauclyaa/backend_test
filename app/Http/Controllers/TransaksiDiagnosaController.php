<?php

namespace App\Http\Controllers;

use App\Models\TransaksiDiagnosa;
use App\Http\Requests\StoreTransaksiDiagnosaRequest;
use App\Http\Requests\UpdateTransaksiDiagnosaRequest;

class TransaksiDiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksiDiagnosaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransaksiDiagnosaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiDiagnosa  $transaksiDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function show(TransaksiDiagnosa $transaksiDiagnosa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransaksiDiagnosa  $transaksiDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function edit(TransaksiDiagnosa $transaksiDiagnosa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksiDiagnosaRequest  $request
     * @param  \App\Models\TransaksiDiagnosa  $transaksiDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTransaksiDiagnosaRequest $request, TransaksiDiagnosa $transaksiDiagnosa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiDiagnosa  $transaksiDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function destroy(TransaksiDiagnosa $transaksiDiagnosa)
    {
        //
    }
}
