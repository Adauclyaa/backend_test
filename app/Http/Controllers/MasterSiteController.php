<?php

namespace App\Http\Controllers;

use App\Models\MasterSite;
use App\Http\Requests\StoreMasterSiteRequest;
use App\Http\Requests\UpdateMasterSiteRequest;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class MasterSiteController extends Controller
{

    public function create()
    {
    }

    public function store(StoreMasterSiteRequest $request)
    {
        $data = $request->only('siteid', 'sitename', 'sitetype', 'companyid', 'sitecompanyname', 'siteaddress', 'sitephone', 'sitefax', 'sitefilename', 'status', 'created_user', 'lastupdate_user');
        $validator = Validator::make($data, [
            'siteid' => 'required|string',
            'sitename' => 'required|string',
            'sitetype' => 'required|string',
            'companyid' => 'required|numeric',
            'sitecompanyname' => 'string|nullable',
            'siteaddress' => 'string|nullable',
            'sitephone' => 'numeric|nullable',
            'sitefax' => 'numeric|nullable',
            'sitefilename' => 'string|nullable',
            'status' => 'string|nullable',
            'created_user' => 'string|nullable',
            'lastupdate_user' => 'string|nullable'
        ]);


        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        $site = MasterSite::create([
            'siteid' => $request->siteid,
            'sitename' => $request->sitename,
            'sitetype' => $request->sitetype,
            'companyid' => $request->companyid,
            'sitecompanyname' => $request->sitecompanyname,
            'siteaddress' => $request->siteaddress,
            'sitephone' => $request->sitephone,
            'sitefax' => $request->sitefax,
            'sitefilename' => $request->sitefilename,
            'status' => $request->status,
            'created_user' => $request->created_user,
            'lastupdate_user' => $request->lastupdate_user,
        ]);

        //Product created, return success response
        return response()->json([
            'status' => true,
            'message' => 'Site created successfully',
            'data' => $site
        ], Response::HTTP_OK);
    }

    public function show()
    {
        $addRelationConstraint = false;
        $data = QueryBuilder::for(MasterSite::class)
        ->allowedFilters([
            AllowedFilter::partial('sitename'),
            AllowedFilter::exact('siteid'),
            AllowedFilter::exact('id'),
            AllowedFilter::exact('companyid'),
            AllowedFilter::exact('tenantid'),
            AllowedFilter::exact('sitetype'),
        ])
        ->get();

        return response()->json($data);
    }

    public function edit(MasterSite $masterSite)
    {
    }

    public function update(UpdateMasterSiteRequest $request, MasterSite $masterSite, $id)
    {

        $data = $request->only('siteid', 'sitename', 'sitetype', 'companyid', 'sitecompanyname', 'siteaddress', 'sitephone', 'sitefax', 'sitefilename', 'status', 'created_user', 'lastupdate_user');
        $validator = Validator::make($data, [
            'siteid' => 'string',
            'sitename' => 'string',
            'sitetype' => 'string',
            'companyid' => 'numeric',
            'sitecompanyname' => 'string|nullable',
            'siteaddress' => 'string|nullable',
            'sitephone' => 'numeric|nullable',
            'sitefax' => 'numeric|nullable',
            'sitefilename' => 'string|nullable',
            'status' => 'string|nullable',
            'created_user' => 'string|nullable',
            'lastupdate_user' => 'string|nullable'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        try {
            $mastersite = MasterSite::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        $site = $mastersite->update([
            'siteid' => $request->siteid,
            'sitename' => $request->sitename,
            'sitetype' => $request->sitetype,
            'companyid' => $request->companyid,
            'sitecompanyname' => $request->sitecompanyname,
            'siteaddress' => $request->siteaddress,
            'sitephone' => $request->sitephone,
            'sitefax' => $request->sitefax,
            'sitefilename' => $request->sitefilename,
            'status' => $request->status,
            'created_user' => $request->created_user,
            'lastupdate_user' => $request->lastupdate_user,
        ]);

        if($site){
            return response()->json([
                'status' => true,
                'code' => Response::HTTP_OK,
                'message' => 'Site updated successfully',
                'data' => $site
            ], Response::HTTP_OK);
        }else{
            return response()->json([
                'status' => false,
                'code' => Response::HTTP_OK,
                'message' => 'Site updated failed',
                'data' => $site
            ], Response::HTTP_OK);
        }

    }

    public function destroy(MasterSite $masterSite, $id)
    {
        try {
            $mastersite = MasterSite::findOrFail($id);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json([
                'status' => false,
                'code' => 404,
                'message' => 'Record not found'
            ], 404);
        }

        $mastersite->delete();
        return response()->json([
            'status' => true,
            'code' => Response::HTTP_OK,
            'message' => 'Site deleted successfully'
        ], Response::HTTP_OK);
    }
}
