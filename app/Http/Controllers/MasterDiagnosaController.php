<?php

namespace App\Http\Controllers;

use App\Models\MasterDiagnosa;
use App\Http\Requests\StoreMasterDiagnosaRequest;
use App\Http\Requests\UpdateMasterDiagnosaRequest;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;
use Spatie\QueryBuilder\QueryBuilder;
use Spatie\QueryBuilder\AllowedFilter;

class MasterDiagnosaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMasterDiagnosaRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMasterDiagnosaRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterDiagnosa  $masterDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function show(MasterDiagnosa $masterDiagnosa)
    {
        $data = QueryBuilder::for(MasterDiagnosa::class)
        ->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('diagnosa'),
            AllowedFilter::exact('tarif'),
        ])
        ->get();

        return response()->json($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterDiagnosa  $masterDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function edit(MasterDiagnosa $masterDiagnosa)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMasterDiagnosaRequest  $request
     * @param  \App\Models\MasterDiagnosa  $masterDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateMasterDiagnosaRequest $request, MasterDiagnosa $masterDiagnosa)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterDiagnosa  $masterDiagnosa
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterDiagnosa $masterDiagnosa)
    {
        //
    }
}
