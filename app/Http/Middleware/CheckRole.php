<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Illuminate\Http\Request;
use App\Models\User;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, string $role)
    {
        $user = JWTAuth::parseToken()->authenticate();
        $roles =User::join('users_groups', 'users_groups.user_id', '=', 'users.id')
        ->join('groups', 'groups.id', '=', 'users_groups.group_id')
        ->select('groups.name', 'users_groups.user_id')
        ->where('users.id', $user->id)
        ->get();

        if($role == "internal"){
            $allowed_group = ['superadmin', 'admin'];
        }else{
            $allowed_group = ['tenant', 'kontraktor'];
        }

        foreach ($roles as $key => $value) {
            if(!in_array($value->name, $allowed_group)){
                return response()->json(['status' => false, 'code' => 403 , 'message' => '403 Forbidden, Cannot Access']);
            }else{
                return $next($request);
            }
        }
    }
}
