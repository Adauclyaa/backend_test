<?php

    use App\Models\Credential;
    use App\Models\Tenant;

    function aws_link(){
        $credential = Credential::first();
        if($credential){

            $bucket = $credential->bucket;
            $url = $credential->url;

            $ciphering = $credential->ciphering;
            $options = $credential->options;
            $encryption_iv = $credential->encryption_iv;
            $encryption_key = $credential->encryption_key;

            $decryption_url = openssl_decrypt($url, $ciphering, $encryption_key, $options, $encryption_iv);
        }
        return $decryption_url;
    }

    function aws_attachment_legal()
    {
        return 'attachment_tenant/';
    }

    function aws_attachment_leasing()
    {
        return 'attachment_leasing/';
    }

    function aws_attachment_pengalihan()
    {
        return 'pengalihan/';
    }

    function aws_attachment_logo()
    {
        return 'logo/';
    }

