<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('companyname',100)->nullable();
            $table->string('phone')->nullable();
            $table->string('email');
            $table->string('username')->unique();
            $table->string('password');
            $table->string('usertype')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();
            $table->string('departmentid')->nullable();
            $table->string('siteid')->nullable();
            $table->bigInteger('refid')->nullable();
            $table->boolean('refdefault')->nullable();
            $table->string('status')->nullable();
            $table->string('created_user')->nullable();
            $table->dateTime('created_datetime')->nullable();
            $table->string('lastupdate_user')->nullable();
            $table->dateTime('lastupdate_datetime')->nullable();
            $table->integer('tenantid')->nullable();
            $table->integer('tenantcompanyid')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
