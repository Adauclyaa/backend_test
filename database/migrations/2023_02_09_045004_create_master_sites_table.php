<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mssite', function (Blueprint $table) {
            $table->id();
            $table->string('siteid',5)->unique();
            $table->string('sitename')->nullable();
            $table->string('sitetype',50)->nullable();
            $table->bigInteger('companyid');
            $table->string('sitecompanyname',100)->nullable();
            $table->text('siteaddress')->nullable();
            $table->string('sitephone',100)->nullable();
            $table->string('sitefax',100)->nullable();
            $table->string('sitefilename',100)->nullable();
            $table->string('status',1)->nullable();
            $table->string('created_user',50)->nullable();
            $table->dateTime('created_datetime')->nullable();
            $table->string('lastupdate_user',50)->nullable();
            $table->dateTime('lastupdate_datetime')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mssite');
    }
}
