/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100427
 Source Host           : localhost:3306
 Source Schema         : apptest

 Target Server Type    : MySQL
 Target Server Version : 100427
 File Encoding         : 65001

 Date: 24/05/2023 15:14:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_registrasi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of customer
-- ----------------------------
INSERT INTO `customer` VALUES (1, 'syam', 'jl jalan', NULL, '2023-05-24 07:55:19', NULL, '2023-05-24 07:55:19', NULL);
INSERT INTO `customer` VALUES (2, 'syam', 'jalan jalan', NULL, '2023-05-24 07:55:24', NULL, '2023-05-24 07:55:24', NULL);
INSERT INTO `customer` VALUES (3, 'testa', 'dawdawd', NULL, '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `customer` VALUES (4, 'adwadd', 'dadawddad', NULL, '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `customer` VALUES (5, 'adwaadwad', 'dawdadwd', NULL, '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);

-- ----------------------------
-- Table structure for failed_jobs
-- ----------------------------
DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE `failed_jobs`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `failed_jobs_uuid_unique`(`uuid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of failed_jobs
-- ----------------------------

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'superadmin', 'Super Administrator');
INSERT INTO `groups` VALUES (2, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (3, 'tenant', 'Tenant');
INSERT INTO `groups` VALUES (4, 'kontraktor', 'Kontraktor');

-- ----------------------------
-- Table structure for master_diagnosa
-- ----------------------------
DROP TABLE IF EXISTS `master_diagnosa`;
CREATE TABLE `master_diagnosa`  (
  `id` int NOT NULL,
  `diagnosa` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tarif` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_diagnosa
-- ----------------------------
INSERT INTO `master_diagnosa` VALUES (1, 'E COLI', '63000', NULL, 1, NULL, 1);
INSERT INTO `master_diagnosa` VALUES (2, 'SALMONELA', '50000', NULL, 1, NULL, 1);
INSERT INTO `master_diagnosa` VALUES (3, 'JAMUR', '60000', NULL, 1, NULL, 1);
INSERT INTO `master_diagnosa` VALUES (4, 'PAKET KIMIA FOOD SECURITY', '61000', NULL, 1, NULL, 1);

-- ----------------------------
-- Table structure for master_sampel_air
-- ----------------------------
DROP TABLE IF EXISTS `master_sampel_air`;
CREATE TABLE `master_sampel_air`  (
  `id` int NOT NULL,
  `parameter` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tarif` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of master_sampel_air
-- ----------------------------
INSERT INTO `master_sampel_air` VALUES (1, 'PAKET PEMERIKSAAN AIR MINUM', '464000', NULL, 1, NULL, 1);
INSERT INTO `master_sampel_air` VALUES (2, 'PAKET PEMERIKSAAN AIR BERSIH', '420000', NULL, 1, NULL, 1);
INSERT INTO `master_sampel_air` VALUES (3, 'PEMERIKSAAN AIR LIMBAH RS', '257000', NULL, 1, NULL, 1);
INSERT INTO `master_sampel_air` VALUES (4, 'PEMERIKSAAN AIR SUNGAI', '513000', NULL, 1, NULL, 1);
INSERT INTO `master_sampel_air` VALUES (5, 'PAKET PEMERIKSAAN AIR KOLAM RENANG', '321000', NULL, 1, NULL, 1);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2019_08_19_000000_create_failed_jobs_table', 1);
INSERT INTO `migrations` VALUES (4, '2019_12_14_000001_create_personal_access_tokens_table', 1);
INSERT INTO `migrations` VALUES (5, '2023_02_09_032543_create_tenants_table', 1);
INSERT INTO `migrations` VALUES (6, '2023_02_09_032612_create_tenant_companies_table', 1);
INSERT INTO `migrations` VALUES (7, '2023_02_09_045004_create_master_sites_table', 1);
INSERT INTO `migrations` VALUES (8, '2023_02_09_070131_create_master_tenant_categories_table', 1);
INSERT INTO `migrations` VALUES (9, '2023_02_09_083006_create_groups_table', 1);
INSERT INTO `migrations` VALUES (10, '2023_02_13_024344_usergroups', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for personal_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE `personal_access_tokens`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `personal_access_tokens_token_unique`(`token`) USING BTREE,
  INDEX `personal_access_tokens_tokenable_type_tokenable_id_index`(`tokenable_type`, `tokenable_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of personal_access_tokens
-- ----------------------------

-- ----------------------------
-- Table structure for transaksi_diagnosa
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_diagnosa`;
CREATE TABLE `transaksi_diagnosa`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `transaksiid` int NULL DEFAULT NULL,
  `diagnosaid` int NULL DEFAULT NULL,
  `qty` int NULL DEFAULT NULL,
  `total` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi_diagnosa
-- ----------------------------
INSERT INTO `transaksi_diagnosa` VALUES (1, 24, 1, 1, '1', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (2, 24, 2, 1, '1', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (3, 24, 3, 0, '0', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (4, 24, 4, 0, '0', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (5, 25, 1, 1, '1', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (6, 25, 2, 1, '1', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (7, 25, 3, 0, '0', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (8, 25, 4, 0, '0', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (9, 26, 1, 1, '1', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (10, 26, 2, 1, '1', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (11, 26, 3, 0, '0', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (12, 26, 4, 0, '0', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (13, 27, 1, 1, '1', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (14, 27, 2, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (15, 27, 3, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (16, 27, 4, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (17, 28, 1, 1, '1', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (18, 28, 2, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (19, 28, 3, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_diagnosa` VALUES (20, 28, 4, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);

-- ----------------------------
-- Table structure for transaksi_formulir
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_formulir`;
CREATE TABLE `transaksi_formulir`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `userid` int NULL DEFAULT NULL,
  `customerid` int NULL DEFAULT NULL,
  `nomor_registrasi` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `tanggal_registrasi` datetime NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `status` enum('draft','completed') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 29 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi_formulir
-- ----------------------------
INSERT INTO `transaksi_formulir` VALUES (21, 1, NULL, 'R0021', NULL, '2023-05-24 06:20:54', NULL, '2023-05-24 06:20:54', NULL, 'draft');
INSERT INTO `transaksi_formulir` VALUES (22, 1, NULL, 'R0022', NULL, '2023-05-24 06:21:16', NULL, '2023-05-24 06:21:16', NULL, 'draft');
INSERT INTO `transaksi_formulir` VALUES (23, 1, NULL, 'R0023', NULL, '2023-05-24 06:21:29', NULL, '2023-05-24 06:21:29', NULL, 'draft');
INSERT INTO `transaksi_formulir` VALUES (24, 1, 1, 'R0024', NULL, '2023-05-24 06:21:59', NULL, '2023-05-24 07:55:19', NULL, 'completed');
INSERT INTO `transaksi_formulir` VALUES (25, 1, 2, 'R0025', '2023-05-24 00:00:00', '2023-05-24 07:52:31', NULL, '2023-05-24 07:55:24', NULL, 'completed');
INSERT INTO `transaksi_formulir` VALUES (26, 1, 3, 'R0026', '2023-05-23 00:00:00', '2023-05-24 07:56:16', NULL, '2023-05-24 07:56:34', NULL, 'completed');
INSERT INTO `transaksi_formulir` VALUES (27, 1, 4, 'R0027', NULL, '2023-05-24 07:58:28', NULL, '2023-05-24 07:58:40', NULL, 'completed');
INSERT INTO `transaksi_formulir` VALUES (28, 1, 5, 'R0028', '2023-05-25 00:00:00', '2023-05-24 07:59:04', NULL, '2023-05-24 07:59:17', NULL, 'completed');

-- ----------------------------
-- Table structure for transaksi_sampel_air
-- ----------------------------
DROP TABLE IF EXISTS `transaksi_sampel_air`;
CREATE TABLE `transaksi_sampel_air`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `transaksiid` int NULL DEFAULT NULL,
  `sampelid` int NULL DEFAULT NULL,
  `qty` int NULL DEFAULT NULL,
  `total` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of transaksi_sampel_air
-- ----------------------------
INSERT INTO `transaksi_sampel_air` VALUES (1, 24, 1, 1, '1', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (2, 24, 2, 2, '2', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (3, 24, 3, 0, '0', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (4, 24, 4, 0, '0', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (5, 24, 5, 0, '0', '2023-05-24 07:42:42', NULL, '2023-05-24 07:42:42', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (6, 25, 1, 1, '1', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (7, 25, 2, 1, '1', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (8, 25, 3, 0, '0', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (9, 25, 4, 0, '0', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (10, 25, 5, 0, '0', '2023-05-24 07:53:47', NULL, '2023-05-24 07:53:47', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (11, 26, 1, 1, '1', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (12, 26, 2, 1, '1', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (13, 26, 3, 0, '0', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (14, 26, 4, 0, '0', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (15, 26, 5, 0, '0', '2023-05-24 07:56:34', NULL, '2023-05-24 07:56:34', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (16, 27, 1, 1, '1', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (17, 27, 2, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (18, 27, 3, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (19, 27, 4, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (20, 27, 5, 0, '0', '2023-05-24 07:58:40', NULL, '2023-05-24 07:58:40', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (21, 28, 1, 1, '1', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (22, 28, 2, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (23, 28, 3, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (24, 28, 4, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);
INSERT INTO `transaksi_sampel_air` VALUES (25, 28, 5, 0, '0', '2023-05-24 07:59:17', NULL, '2023-05-24 07:59:17', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `status` enum('A','N','I') CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'active and inactive',
  `created_by` int NULL DEFAULT NULL,
  `created_at` datetime NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  `updated_at` datetime NULL DEFAULT NULL,
  `notification` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_username_unique`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 43 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'Super Admin', NULL, 'admin@admin.com', 'superadmin', '$2y$10$6e01IM0g6qK/gbAXK20v3e.phyyz2LmRDGSwFZEibcN8PEnFRTflq', NULL, NULL, 'A', NULL, NULL, 1, '2023-03-29 01:52:29', 'Y');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `group_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 57 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (12, 16, 3, '2023-02-27 02:40:08', '2023-02-27 02:40:08');
INSERT INTO `users_groups` VALUES (13, 17, 3, '2023-02-27 03:41:34', '2023-02-27 03:41:34');
INSERT INTO `users_groups` VALUES (14, 2, 2, '2023-03-01 04:44:10', '2023-03-01 04:44:10');
INSERT INTO `users_groups` VALUES (15, 3, 2, '2023-03-01 04:44:57', '2023-03-01 04:44:57');
INSERT INTO `users_groups` VALUES (20, 8, 2, '2023-03-01 04:54:25', '2023-03-01 04:54:25');
INSERT INTO `users_groups` VALUES (21, 9, 2, '2023-03-01 04:54:55', '2023-03-01 04:54:55');
INSERT INTO `users_groups` VALUES (22, 10, 2, '2023-03-01 04:55:45', '2023-03-01 04:55:45');
INSERT INTO `users_groups` VALUES (23, 11, 2, '2023-03-01 04:56:23', '2023-03-01 04:56:23');
INSERT INTO `users_groups` VALUES (24, 12, 2, '2023-03-01 05:04:08', '2023-03-01 05:04:08');
INSERT INTO `users_groups` VALUES (44, 1, 1, '2023-03-03 06:38:46', '2023-03-03 06:38:46');
INSERT INTO `users_groups` VALUES (45, 35, 3, '2023-03-07 02:37:26', '2023-03-07 02:37:26');
INSERT INTO `users_groups` VALUES (46, 36, 3, '2023-03-07 02:45:42', '2023-03-07 02:45:42');
INSERT INTO `users_groups` VALUES (47, 37, 3, '2023-03-07 02:47:31', '2023-03-07 02:47:31');
INSERT INTO `users_groups` VALUES (48, 4, 2, '2023-03-07 07:16:37', '2023-03-07 07:16:37');
INSERT INTO `users_groups` VALUES (49, 5, 2, '2023-03-07 07:17:04', '2023-03-07 07:17:04');
INSERT INTO `users_groups` VALUES (50, 6, 2, '2023-03-07 07:17:15', '2023-03-07 07:17:15');
INSERT INTO `users_groups` VALUES (51, 7, 2, '2023-03-07 07:17:28', '2023-03-07 07:17:28');
INSERT INTO `users_groups` VALUES (53, 39, 3, '2023-03-07 09:32:45', '2023-03-07 09:32:45');
INSERT INTO `users_groups` VALUES (54, 40, 3, '2023-03-13 05:05:45', '2023-03-13 05:05:45');
INSERT INTO `users_groups` VALUES (55, 41, 3, '2023-03-17 07:01:50', '2023-03-17 07:01:50');
INSERT INTO `users_groups` VALUES (56, 42, 3, '2023-03-29 01:53:21', '2023-03-29 01:53:21');

SET FOREIGN_KEY_CHECKS = 1;
